from word_freq import *
import pytest

@pytest.mark.parametrize(
        "n,in_text,expected", 
        [
            (0, "",       []),
            (1000, None,  []),
            (1000, "   ", []),
            (1000, "",    []),
            (-1, "a b c", []),
            (3, "c b a",   [("a", 1), ("b", 1), ("c", 1)]),
            (3, "c b a b", [("b", 2), ("a", 1), ("c", 1)]),
            (2, "c b a b", [("b", 2), ("a", 1)]),
            (3, "The sun shines over the lake", [("the", 2), ("lake", 1), ("over", 1)]),
            (4, " c b    A b a  d ",  [("a", 2), ("b", 2), ("c", 1), ("d", 1)]),
            (3, "a word dork WoRd DoRK a", [("a", 2), ("dork", 2), ("word", 2)]),
        ])
def test_most_frequent(n, in_text, expected):
    assert most_frequent(n, in_text) == expected

@pytest.mark.parametrize(
        "in_text,expected", 
        [
            ("      ", 0),
            (None,     0),
            ("",       0),
            ("a b c",  1),
            ("c b a B",    2), 
            ("c b a B bb", 2), 
            ("The sun shines over the lake", 2), 
        ])
def test_max_frequency(in_text, expected):
    assert max_frequency(in_text) == expected

@pytest.mark.parametrize(
        "word,in_text,expected", 
        [
            (None, "",    0),
            ("", None,    0),
            (None, None,  0),
            ("", "",      0),
            (" ", "  ",   0),
            (" ", "text", 0),
            ("two words", "two words", 0),
            ("a", "a b",   1),
            ("b", "a b",   1),
            ("c", "a c b c",  2),
            ("c", "c C a c",  3), 
            ("c", " c   d C a c ",  3), 
            (" Word ", "Word dork word WoRd zork",  3), 
            ("The", "The sun shines over the lake", 2), 
        ])
def test_frequency_of(word, in_text, expected):
    assert frequency_of(word, in_text) == expected

if __name__ == '__main__':
    pytest.main()
