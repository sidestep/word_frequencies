from collections import Counter
from itertools import groupby, chain, islice
from operator import itemgetter

freq_idx = lambda items: Counter(items).most_common()
words = lambda s: s.lower().split() if isinstance(s, str) else []
word_freqs = lambda s: freq_idx(words(s))

def max_frequency(in_string: str):
    w = words(in_string)
    return freq_idx(w)[0][1] if w else 0

def frequency_of(word: str, in_string: str):
    w = words(word)
    if len(w) == 1:
        return dict(word_freqs(in_string)).get(w[0], 0)
    return 0

def most_frequent(n: int, in_string: str):
    if n < 1:
        return []
    grouped_by_count = groupby(word_freqs(in_string), key=itemgetter(1)) 
    sorted_groups = chain(*(sorted(group) for _,group in grouped_by_count))
    return list(islice(sorted_groups, n))
